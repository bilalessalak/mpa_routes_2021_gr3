from app import db
class Article(db.Model):
    nom = db.Column(db.String(140), primary_key=True)
    conditionnement = db.Column(db.String(140))
    def __repr__(self):
        return self.nom + " par " + self.conditionnement
    listes = db.relationship("Association", back_populates="article")
    magasins = db.relationship("EstVendu", back_populates="Article")


class Liste(db.Model):
    nom = db.Column(db.String(140), primary_key=True)
    def __repr__(self):
        return self.nom
    articles = db.relationship("Association", back_populates="liste")

class Association(db.Model):
    __tablename__ = 'association_article_liste'
    left_id = db.Column(db.String, db.ForeignKey('article.nom'), primary_key=True)
    right_id = db.Column(db.String, db.ForeignKey('liste.nom'), primary_key=True)
    quantite = db.Column(db.Float)
    article = db.relationship("Article", back_populates="listes")
    liste = db.relationship("Liste", back_populates="articles")

class Magasin(db.Model):
    nom= db.Column(db.String(140), primary_key=True)
    lieu= db.Column(db.String(140))
    ref= db.Column(db.String(140))
    def __repr__(self):
        return self.nom + " à " + self.lieu + " ref: " + self.ref
    articles = db.relationship("EstVendu", back_populates="Magasin")

class EstVendu(db.Model):
    __tablename__="EstVendu"
    ref_article=db.Column(db.String, db.ForeignKey("Article.nom"), primary_key=True)
    ref_magasin=db.Column(db.String, db.ForeignKey("Magasin.nom_magasin"), "Magasin" primary_key=True)
    prix_unitaire=db.Column(db.Float)
    article= db.relationship("Article", back_populates="Magasin")
    magasin= db.relationship("Magasin", back_populates="Article")